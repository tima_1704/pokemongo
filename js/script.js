const main_item = document.querySelector('.main_item');
const content_header = document.querySelector('.content_header')
const btn_back  = document.querySelector('.btn_back');
const page = document.querySelector('.page');
const btn_next = document.querySelector('.btn_next');
let offsetcounter = 0;
let pageCounter = 1;
let total_pokemons = 1118;
const LIMIT = 20;
const total_pages = Math.floor(total_pokemons / LIMIT);
const base = 'https://pokeapi.co/api/v2';

const Api = (url,quary,cb) => {
    const xhr = new XMLHttpRequest();
    xhr.open('GET',`${url}?${quary}`);
    xhr.addEventListener('load' ,() => {
        const response = JSON.parse(xhr.response);
        cb(response);   
    });
    xhr.addEventListener('error' , err => {
        console.log(err);
    });
    xhr.send();
};

window.addEventListener('load' , ()  => {
    Api(`${base}/pokemon` ,`offset=${offsetcounter}&limit=${LIMIT}` , res => {
        const temp = res.results.map(item => cardTemp(item)).join('');
        main_item.innerHTML = temp
    });
});

function cardTemp(item){
    return `
    <div class="card">
        <div class="card_header">
            <h1>${item.name}</h1>
        </div>
        <div class="card_body">
            <button class="btn_card" onclick="InfoActive('${item.url}')">Click</button>
        </div>
    </div>
    `
};

// info

function InfoActive(url){
    Api(url, " ", item => {
        console.log(item);
        main_item.innerHTML  = `
        <div class="card_info">
            <div class="left_content">
                <img class="hero" src="${item.sprites.other.dream_world.front_default}">
                <div class="down_left_cont">
                    <img src="${item.sprites.back_default}">
                    <img src="${item.sprites.back_shiny}">
                    <img src="${item.sprites.front_shiny}">
                    <img src="${item.sprites.front_default}">
                </div>
            </div>
            <div class="right_content">
                <ul>
                    <h1>${item.name}</h1>
                    <p><span>ability:</span> ${item.abilities[0].ability.name}</p>
                    <p><span>ability:</span> ${item.abilities[1].ability.name}</p>
                    <p><span>experience:</span>${item.base_experience}</p>
                    <p><span>height:</span>${item.height}</p>
                    <p><span>weight:</span>${item.weight}</p>
                    <p><span>base_stat:</span>${item.stats[0].base_stat}</p>
                    <p><span>base_stat:</span>${item.stats[1].base_stat}</p>
                    <p><span>base_stat:</span>${item.stats[2].base_stat}</p>
                    <p><span>base_stat:</span>${item.stats[3].base_stat}</p>
                    <p><span>type one:</span>${item.types[0].type.name}</p>
                    <button class="btnload" onclick="loadfunc()">back</button>
                </ul>
            </div>
        </div>
        `
    });

    btn_next.classList.toggle('activenext');
    btn_back.classList.toggle('activeback');
    page.classList.toggle('activepage');
    input_main.classList.toggle('activeinput');
    content_header.classList.toggle('activeheader');
    main_item.classList.toggle('activemain')
}
function loadfunc(){
    location.reload();
}

// input (error)

const input_main = document.querySelector('.input_main');
input_main.addEventListener('input' , e => {
    const value = e.target.value;
    if(!value){
        cardTemp(`${item.name}` , res => {
            const temp = res.map(item => cardTemp(item)).join(' ');
            main_item.innerHTML = temp;
        })
    }else{
        cardTemp(`${item.name}=${value}` , res => {
            const temp = res.map(item => cardTemp(item)).join(' ');
            main_item.innerHTML = temp;
        })
    }
})

// next-back

window.addEventListener('load', () => {
    page.innerHTML = pageCounter;
    btn_back.setAttribute('disabled' , true);
});

btn_next.addEventListener('click' , e => {
    e.preventDefault();
    btn_back.removeAttribute('disabled');
    if(pageCounter >= 1 && pageCounter <= total_pages){
        if(pageCounter === total_pages){
            btn_next.setAttribute('disabled' , true)
            Api(`${base}/pokemon` , `offset=${offsetcounter += LIMIT}&limit=${LIMIT}` , res => {
                pageCounter++;
                page.innerHTML = pageCounter;
                let temp = res.results.map(item => cardTemp(item)).join('');
                main_item.innerHTML = temp;
            })
        }else{
            Api(`${base}/pokemon` , `offset=${offsetcounter += LIMIT}&limit=${LIMIT}` , res => {
                pageCounter++;
                page.innerHTML = pageCounter;
                let temp = res.results.map(item => cardTemp(item)).join('');
                main_item.innerHTML = temp;
            })
        }
    }
});
btn_back.addEventListener('click' , e => {
    e.preventDefault();
    if(pageCounter > 1){
        pageCounter--;
        if(pageCounter === 1){
            btn_back.setAttribute('disabled' , true);
            offsetcounter = 0;
            page.innerHTML = pageCounter;
            Api(`${base}/pokemon` , `offset=${offsetcounter}&limit=${LIMIT}` , res => {
                let temp = res.results.map(item => cardTemp(item)).join('');
                main_item.innerHTML = temp;
            })
        }else{
            Api(`${base}/pokemon` ,  `offset=${offsetcounter -= LIMIT}&limit=${LIMIT}`, res => {
                page.innerHTML = pageCounter;
                let temp = res.results.map(item => cardTemp(item)).join('');
                main_item.innerHTML = temp; 
            });
        }
    }
});